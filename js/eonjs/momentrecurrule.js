/**
 * 
 */

/**
 * Wraps [moment-recur]{@link https://github.com/c-trimm/moment-recur} recur rule instances,
 * @extends AbstractRecurRule
 */
class MomentRecurRule extends AbstractRecurRule {

    /**
     *
     * @param {Object} [_momentRecur] - See property [recurRule]{@link MomentRecurRule#recurRule}.
     */
    constructor( _momentRecur, _data ) {
        super( _momentRecur, _data );
    }

    /**
     * moment-recur object.
     * @type {Object}
     */
    set recurRule( _moment_recur_obj ) {
        // #todo docs, moment-recur operates on the date only part of the date
        // #todo docs, .fromDate() will be overwritten if set
        super.recurRule = _moment_recur_obj;
        if ( !( this.recurRule instanceof Object ) ) throw new TypeError();
    }

    /****
     * @returns {Object}
     */
    get recurRule() {
        // NB by default from and end dates inclusive, #todo - options
        return super.recurRule;
    }

    match( _matchDate ) {  // abstract method implemented
        if ( !( _matchDate instanceof Date ) ) throw new TypeError();
        let _returnVal;

        _returnVal = this.recurRule.matches( moment( _matchDate ) );

        if ( typeof _returnVal !== 'boolean' ) throw new TypeError();
        return _returnVal;
    }

    after( _fromDate )   // abstract method implemented
    {
        if ( typeof this.recurRule === 'undefined' ) throw new TypeError();
        if ( !( _fromDate instanceof Date ) ) throw new TypeError();
        let _returnVal;
        // .endDate() undefined if not set
        // .next(count ) returns array of Unix offset values (start date or from date )

        let hold = this.recurRule.fromDate();  // #todo clone method issue opened github
        let [ next ] = this.recurRule.fromDate( moment( _fromDate ) ).next( 1 );
        this.recurRule.fromDate( hold );  // in the absence of affor mentioned clone method
        if ( typeof this.recurRule.endDate() === 'undefined' ) {  // .fromDate() ignores .endDate
            _returnVal = new Date( next );  // end date not defined, return next occurrence
        } else if ( this.recurRule.endDate() instanceof Object ) {  // end date defined
            if ( next.valueOf() <= this.recurRule.endDate().valueOf() ) {  // NB inclusive of end date #todo
                _returnVal = new Date( next );  // return next occ.
            } else if ( next.valueOf() > this.recurRule.endDate().valueOf() ) {  // equal to or after end date
                _returnVal = null; // return null
            }
        } else {
            throw new Error();
        }

        if ( !( _returnVal instanceof Date || _returnVal === null ) ) throw new TypeError();
        return _returnVal;
    }

    afterInclusive( _fromDate )  // abstract method implemented
    {
        if ( typeof this.recurRule === 'undefined' ) throw new TypeError();
        if ( !( _fromDate instanceof Date ) ) throw new TypeError();
        let _returnVal;

        // start date defaults to date object instantiated if not set
        if ( this.recurRule.matches( moment( _fromDate ) ) ) {
            _returnVal = new Date( _fromDate );
            if ( !( _returnVal instanceof Date ) ) throw new TypeError();
        } else {
            _returnVal = this.after( _fromDate );
        }

        return _returnVal;
    }

}
