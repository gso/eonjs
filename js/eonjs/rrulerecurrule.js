/**
 * 
 */

/**
 * Wraps [RRuleRecurRule]{@link https://github.com/c-trimm/rrule} recur rule instances,
 * @extends AbstractRecurRule
 */
class RRuleRecurRule extends AbstractRecurRule {

    /**
     *
     * @param {RRule} [_rRule] - See property [recurRule]{@link RRuleRecurRule#recurRule}.
     */
    constructor( _rRule, _data ) {
        super( _rRule, _data );
    }

    /**
     * RRule object.
     * @type {RRule}
     */
    set recurRule( _rRule ) {
        super.recurRule = _rRule;
        if ( !( this.recurRule instanceof rrule.RRule ) ) throw new TypeError();
    }

    /****
     * @returns {RRule}
     */
    get recurRule() {
        return super.recurRule;
    }

    match( _matchDate ) {  // abstract method implemented
        if ( !( _matchDate instanceof Date ) ) throw new TypeError();
        let _returnVal;

        let _matchDateAry = this.recurRule.between( _matchDate, _matchDate, true );  // inclusive
        if ( !( _matchDateAry instanceof Array )) throw new TypeError();

        if ( _matchDateAry.length === 0 ) {
            _returnVal = false;
        } else if ( _matchDateAry.length === 1 ) {
            _returnVal = true;
        } else {
            throw new Error();
        }

        return _returnVal;
    }

    after( _fromDate )  // abstract method implemented
    {
        if ( typeof this.recurRule === 'undefined' ) throw new TypeError();
        if ( !( _fromDate instanceof Date ) ) throw new TypeError();
        let _returnVal;

        _returnVal = this.recurRule.after( _fromDate, false );  // after date (null on none)

        if ( !( _returnVal instanceof Date || _returnVal === null ) ) throw new TypeError();
        return _returnVal;
    }

    afterInclusive( _fromDate )  // abstract method implemented
    {
        if ( typeof this.recurRule === 'undefined' ) throw new TypeError();
        if ( !( _fromDate instanceof Date ) ) throw new TypeError();
        let _returnVal;

        _returnVal = this.recurRule.after( _fromDate, true )  // include from date

        if ( !( _returnVal instanceof Date || _returnVal === null ) ) throw new TypeError();
        return _returnVal;
    }

}
