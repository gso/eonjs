/**
 * Created by user1 on 3/31/15.
 */

// Gererated test data.

// Instantiate rrule objects using 'TestData.js' test data, and
// wrap and encapsulate in a RecurRule object.  Instantiate test RecurRuleContainer
// objects from latter recur rules.

( function () {
    var _periodParams = [];

    TestData.recurRuleLib = "rrule";
    _periodParams[ TestData.periodEnum.days3 ] = { unit: rrule.RRule.DAILY, count: 3 };
    _periodParams[ TestData.periodEnum.days4 ] = { unit: rrule.RRule.DAILY, count: 4 };
    _periodParams[ TestData.periodEnum.days6 ] = { unit: rrule.RRule.DAILY, count: 6 };

    var i = 0, l = TestData.table.length;
    for ( i ; i < l; i++ ) {
        var _recurRuleData = TestData.table[i];


        _recurRuleData.libRecurRule = new rrule.RRule({
            freq: _periodParams[ _recurRuleData.periodEnum ].unit,
            interval: _periodParams[ _recurRuleData.periodEnum ].count,
            dtstart: _recurRuleData.start,
            until: _recurRuleData.end  // this is inclusive
        });
        _recurRuleData.recurRule = new RRuleRecurRule( _recurRuleData.libRecurRule );  // wrapper
    }

    // container test objects

    TestData.container1 = new RecurRuleContainer(
            TestData.table[0].recurRule,
            TestData.table[1].recurRule
    );
    TestData.container2 = new RecurRuleContainer(
            TestData.table[2].recurRule,
            TestData.container1
        );

})();
