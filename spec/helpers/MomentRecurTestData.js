/**
 * Created by user1 on 3/31/15.
 */

// Gererated test data.

// Instantiate moment-recur objects using 'TestData.js' test data, and
// wrap and encapsulate in a RecurRule object.  Instantiate test RecurRuleContainer
// objects from latter recur rules.

( function () {
    var _periodParams = [];

    TestData.recurRuleLib = "moment-recur";
    _periodParams[ TestData.periodEnum.days3 ] = { unit: 'days', count: 3 };
    _periodParams[ TestData.periodEnum.days4 ] = { unit: 'days', count: 4 };
    _periodParams[ TestData.periodEnum.days6 ] = { unit: 'days', count: 6 };

    var i = 0, l = TestData.table.length;
    for ( i ; i < l; i++ ) {
        var _recurRuleData = TestData.table[i];

        _recurRuleData.libRecurRule = moment.recur(  // moment-recur object
                moment(_recurRuleData.start),
                moment(_recurRuleData.end)
            ).every(
                _periodParams[ _recurRuleData.periodEnum ].count,
                _periodParams[ _recurRuleData.periodEnum ].unit
            );
        _recurRuleData.recurRule = new MomentRecurRule( _recurRuleData.libRecurRule );  // wrapper
    }

    // container test objects

    TestData.container1 = new RecurRuleContainer(
            TestData.table[0].recurRule,
            TestData.table[1].recurRule
    );
    TestData.container2 = new RecurRuleContainer(
            TestData.table[2].recurRule,
            TestData.container1
        );

})();
