
describe('A RecurRuleContainer object', function () {
    
    var recurRule1StartDate = TestData.table[0].start;
    var recurRule2StartDate = TestData.table[1].start;
    var recurRule3StartDate = TestData.table[2].start;
    
    var recurRule1Recurrence1 = TestData.table[0].recurrenceAry[0];
    var recurRule1Recurrence2 = TestData.table[0].recurrenceAry[1];
    var recurRule1Recurrence3 = TestData.table[0].recurrenceAry[2];
    var recurRule1Recurrence4 = TestData.table[0].recurrenceAry[3];
    var recurRule2Recurrence1 = TestData.table[1].recurrenceAry[0];
    var recurRule2Recurrence2 = TestData.table[1].recurrenceAry[1];
    var recurRule3Recurrence1 = TestData.table[2].recurrenceAry[0];
    var recurRule3Recurrence2 = TestData.table[2].recurrenceAry[1];
    
    var recurRule1 = TestData.table[0].recurRule;
    var recurRule2 = TestData.table[1].recurRule;
    var recurRule3 = TestData.table[2].recurRule;
    
    var container1 = TestData.container1;  // contains rules 1 & 2
    var container2 = TestData.container2;  // contains rule 3 and container 1

    it( "is created with a validating constructor", function () {
        expect( function () {
                new RecurRuleContainer( "should be an occurrence object" );
            } )
            .toThrowError( TypeError );
        expect( container1 instanceof RecurRuleContainer ).toBe( true );
        expect( container2 instanceof RecurRuleContainer ).toBe( true );
    } );

    it( "provides a method to add a recur rule to the container", function () {
        container1.add( recurRule3 );
        expect( container1.count() ).toEqual( 3 );
        expect( container1.contains( recurRule3 ) ).toBe( true );
        container1.remove( recurRule3 );  // remove recur rule!  #todo
        expect( function () {
                container2.add( recurRule3 );  // not unique in set
            } )
            .toThrowError( Error );
    } );

    it( "provides a method to spec if a recur rule exists in the container", function () {
        expect( container1.contains( recurRule2 ) ).toBe( true );
        expect( container2.contains( container1 ) ).toBe( true );
    } );

    it( "provides a method to count the number of recur rules in the container", function () {
        expect( container2.count() ).toEqual( 2 );
    } );

    it( "provides a method to delete a recur rule from the container", function () {
        container1.remove( recurRule2 );
        expect( container1.count() ).toEqual( 1 );
        expect( container1.contains(recurRule2) ).toBe( false );
        container1.add(recurRule2);  // put recur rule back!  #todo
    } );

    it( "will return true if any one of the recur rules in the container recurs on a given date", function () {
        expect( container2.match( recurRule1StartDate ) ).toBe( true );
        expect( container2.match(
            new Date (recurRule1StartDate.getTime() - TestData.const.dayMilliSeconds)
        ) ).toBe( false );
    } );

    it( "returns an iterator yielding Occurrence objects for recur rules in the container that occur on a given date",
            function () {
        var _next, _itr = container2.occurrenceMatch( new Date(2015, 0, 7) );
        var _ary = [];
        while ( ( _next = _itr.next() ) && !_next.done ) {
            _ary.push( _next.value );
        }
        expect( _ary.length ).toEqual( 2 );
        expect( _ary[0].recurRule ).toBe( recurRule3 );
        expect( _ary[1].recurRule ).toBe( recurRule1 );
        expect( _ary[0].date.getTime() ).toBe( recurRule3Recurrence2.getTime() );
        expect( _ary[1].date.getTime() ).toBe( recurRule1Recurrence3.getTime() );
    } );

    it( "returns an iterator yielding the recurrence date of each recur rule in the container following " +
            "a given date", function () {
        var _next, _itr = container2.recurRuleRecurrenceAfter( new Date(2015, 0, 1) );
        var _ary = [];
        while ( ( _next = _itr.next() ) && !_next.done ) {
            _ary.push( _next.value );
        }
        expect( _ary.length ).toEqual( 3 );
        expect( _ary[0].recurRule ).toBe( recurRule3 );
        expect( _ary[1].recurRule ).toBe( recurRule1 );
        expect( _ary[2].recurRule ).toBe( recurRule2 );
        expect( _ary[0].date.getTime() ).toEqual( recurRule3Recurrence2.getTime() );
        expect( _ary[1].date ).toEqual( recurRule1Recurrence2 );
        expect( _ary[2].date.getTime() ).toEqual( recurRule2Recurrence2.getTime() );
    } );

    it( "returns an iterator yielding the recurrence date of each recur rule in the container following " +
            "a given date (inclusive of the date)", function () {
        var recurRuleItr, _next, _ary = [];
        recurRuleItr = container2.recurRuleRecurrenceAfterInclusive( new Date(2015, 0, 1) );
        while ( ( _next = recurRuleItr.next() ) && !_next.done ) {
            _ary.push( _next.value );
        }
        expect( _ary.length ).toEqual( 3 );
        expect( _ary[0].date.getTime() ).toEqual( recurRule3Recurrence1.getTime() );
        expect( _ary[1].date.getTime() ).toEqual( recurRule1Recurrence1.getTime() );
        expect(_ary[2].date.getTime() ).toEqual( recurRule2Recurrence1.getTime() );
        expect( _ary[0].recurRule ).toBe( recurRule3 );
        expect( _ary[1].recurRule ).toBe( recurRule1 );
        expect( _ary[2].recurRule ).toBe( recurRule2 );
    } );

    it( "returns the next recurrence following a given date of recur rules in the container", function () {
        var _queryDate = new Date(2015, 0, 7);
        var _expectedDate =  new Date(2015, 0, 9);
        expect( _expectedDate.getTime() ).toEqual( container2.after( _queryDate ).getTime() );
    } );

    it( "returns the next recurrence following a given inclusive date of recur rules in the container", function () {
        var _queryDate = new Date(2015, 0, 7);
        var _expectedDate =  new Date(2015, 0, 7);
        expect( _expectedDate.getTime() ).toEqual( container2.afterInclusive( _queryDate ).getTime() );
    } );

    it( "returns an iterator yielding Occurrence objects of recur rules recurring next in the container " +
            "following a given date", function () {
        var _queryDate = new Date(2015, 0, 5);
        var _expectedDate =  new Date(2015, 0, 7);
        var _next, _itr = container2.occurrenceAfter( _queryDate );
        var _ary = [];
        while ( ( _next = _itr.next() ) && !_next.done ) {
            _ary.push( _next.value );
        }
        expect( _ary.length ).toEqual( 2 );
        expect( _ary[0].date.getTime() ).toEqual( _expectedDate.getTime() );
        expect( _ary[1].date.getTime() ).toEqual( _expectedDate.getTime() );
        expect( _ary[0].recurRule ).toBe( recurRule3 );
        expect( _ary[1].recurRule ).toBe( recurRule1 );
    } );

    it( "returns an iterator yielding Occurrence objects of recur rules recurring next in the container " +
            "following a given date (inclusive of the date)", function () {
        var _queryDate = new Date(2015, 0, 7);
        var _expectedDate =  new Date(2015, 0, 7);
        var _next, _itr = container2.occurrenceAfterInclusive( _queryDate );
        var _ary = [];
        while ( ( _next = _itr.next() ) && !_next.done ) {
            _ary.push( _next.value );
        }
        expect( _ary.length ).toEqual( 2 );
        expect( _ary[0].date.getTime() ).toEqual( _expectedDate.getTime() );
        expect( _ary[1].date.getTime() ).toEqual( _expectedDate.getTime() );
        expect( _ary[0].recurRule ).toBe( recurRule3 );
        expect( _ary[1].recurRule ).toBe( recurRule1 );
    } );

    it( "returns an iterator yielding each recur rule in the container descent first left to right", function () {
        var _next, _itr = container2.recurRule();
        var _ary = [];
        while ( ( _next = _itr.next() ) && !_next.done ) {
            _ary.push( _next.value );
        }
        expect( _ary.length ).toEqual( 3 );
        expect( _ary[0] ).toBe( recurRule3 );
        expect( _ary[1] ).toBe( recurRule1 );
        expect( _ary[2] ).toBe( recurRule2 );
    } );

    it( "returns an iterator yeilding the set of recurrance dates of recur rules in the container " +
            "between a given a start and ending date (inclusive)", function () {
        var _next, _itr = container2.occurrence( TestData.testRun.startDate, TestData.testRun.endDate );
        var _ary = [];
        while ( ( _next = _itr.next() ) && !_next.done ) {
            _ary.push( _next.value );
        }
        expect( _ary.length ).toEqual( 7 );
        expect( _ary[0].getTime() ).toEqual( ( new Date( 2015, 0, 1 ) ).getTime() );
        expect( _ary[1].getTime() ).toEqual( ( new Date( 2015, 0, 4 ) ).getTime() );
        expect( _ary[2].getTime() ).toEqual( ( new Date( 2015, 0, 5 ) ).getTime() );
        expect( _ary[3].getTime() ).toEqual( ( new Date( 2015, 0, 7 ) ).getTime() );
        expect( _ary[4].getTime() ).toEqual( ( new Date( 2015, 0, 9 ) ).getTime() );
        expect( _ary[5].getTime() ).toEqual( ( new Date( 2015, 0, 10 ) ).getTime() );
        expect( _ary[6].getTime() ).toEqual( ( new Date( 2015, 0, 13 ) ).getTime() );
    } );

    it( "returns an iterator yeilding the set of Occurence objects for recur rules in the container " +
            "between a given a start and ending date (inclusive)", function () {
        var _next,
            _itr = container2.recurrence( TestData.testRun.startDate, TestData.testRun.endDate );
        var _ary = [];
        while ( ( _next = _itr.next() ) && !_next.done ) {
            _ary.push( _next.value );
        }
        expect( _ary.length ).toEqual( 12 );
        //_ary.forEach( function ( el ) { expect( el instanceof Occurrence ).toBe( true ) } );
        _ary.forEach( function ( el ) { expect( el.date instanceof Date ).toBe( true ) } );
        expect( _ary[0].date.getTime() ).toEqual( ( new Date( 2015, 0, 1 ) ).getTime() );
        expect( _ary[1].date.getTime() ).toEqual( ( new Date( 2015, 0, 1 ) ).getTime() );
        expect( _ary[2].date.getTime() ).toEqual( ( new Date( 2015, 0, 1 ) ).getTime() );
        expect( _ary[3].date.getTime() ).toEqual( ( new Date( 2015, 0, 4 ) ).getTime() );
        expect( _ary[4].date.getTime() ).toEqual( ( new Date( 2015, 0, 5 ) ).getTime() );
        expect( _ary[5].date.getTime() ).toEqual( ( new Date( 2015, 0, 7 ) ).getTime() );
        expect( _ary[6].date.getTime() ).toEqual( ( new Date( 2015, 0, 7 ) ).getTime() );
        expect( _ary[7].date.getTime() ).toEqual( ( new Date( 2015, 0, 9 ) ).getTime() );
        expect( _ary[8].date.getTime() ).toEqual( ( new Date( 2015, 0, 10 ) ).getTime() );
        expect( _ary[9].date.getTime() ).toEqual( ( new Date( 2015, 0, 13 ) ).getTime() );
        expect( _ary[10].date.getTime() ).toEqual( ( new Date( 2015, 0, 13 ) ).getTime() );
        expect( _ary[11].date.getTime() ).toEqual( ( new Date( 2015, 0, 13 ) ).getTime() );
        //_ary.forEach( function ( el ) { expect( el.recurRule instanceof AbstractRecurRule ).toBe( true ) } );
        expect( _ary[0].recurRule ).toBe( recurRule3 );  // every 6 days
        expect( _ary[1].recurRule ).toBe( recurRule1 );  // every 3 days
        expect( _ary[2].recurRule ).toBe( recurRule2 );  // every 4 days
        expect( _ary[3].recurRule ).toBe( recurRule1 );  // day 4
        expect( _ary[4].recurRule ).toBe( recurRule2 );  // day 5
        expect( _ary[5].recurRule ).toBe( recurRule3 );  // day 7
        expect( _ary[6].recurRule ).toBe( recurRule1 );  // day 7
        expect( _ary[7].recurRule ).toBe( recurRule2 );  // day 9
        expect( _ary[8].recurRule ).toBe( recurRule1 );  // day 10
        expect( _ary[9].recurRule ).toBe( recurRule3 );  // day 13
        expect( _ary[10].recurRule ).toBe( recurRule1 );  // day 13
        expect( _ary[11].recurRule ).toBe( recurRule2 );  // day 13
    } );

});
