
describe('An AbstractRecurRule recur rule subclass', function () {

    var recurRule1 = TestData.table[0].recurRule;

    var recurRule1StartDate = TestData.table[0].start;
    var recurRule1EndDate = TestData.table[0].end;
    var recurRule2StartDate = TestData.table[1].start;
    var recurRule3StartDate = TestData.table[2].start;

    var recurRule1Recurrence2 = TestData.table[0].recurrenceAry[1];  // recurrence after initial start date
    var recurRule1Recurrence3 = TestData.table[0].recurrenceAry[2];  // recurrence after initial start date
    var recurRule1Recurrence4 = TestData.table[0].recurrenceAry[3];  // recurrence after initial start date
    var recurRule1Recurrence5 = TestData.table[0].recurrenceAry[4];  // recurrence after initial start date

    it( "is an instance of AbstractRecurRule", function () {
        expect( recurRule1 instanceof AbstractRecurRule ).toBe( true );
    } );

    it( "will spec if the recur rule recurs on a given date", function () {
        // returns true if falls on given date
        expect( recurRule1.match( recurRule1StartDate ) ).toBe( true );
        // returns false if does not fall on given date
        expect( recurRule1.match(
                new Date ( recurRule1StartDate.getTime() - TestData.const.dayMilliSeconds )
            ) ).toBe( false );
    } );

    it( "returns the next recurrence following a given date", function() {
        var _queryDate;
        // excludes a recurrence on the given date
        _queryDate = recurRule1.after( recurRule1StartDate );
        expect( recurRule1Recurrence2.getTime() ).toEqual( _queryDate.getTime() );
        // returns the recurrence date following the given date
        _queryDate = new Date( recurRule1StartDate.getTime() + TestData.const.dayMilliSeconds );
        expect( recurRule1Recurrence2.getTime() ).toEqual( recurRule1.after(_queryDate).getTime() );
        // returns null if the given date falls after the recur rule end date
        _queryDate = new Date( recurRule1EndDate.getTime() + TestData.const.dayMilliSeconds );
        expect( recurRule1.after(_queryDate) ).toBeNull();
    } );

    it( "returns the next recurrence following a given date inclusive", function() {
        var _queryDate;
        // includes a recurrence on the given date
        _queryDate = recurRule1.afterInclusive( recurRule1StartDate );
        expect( recurRule1StartDate.getTime() ).toEqual( _queryDate.getTime() );
        // returns the recurrence date following (nlcusive) the given date otherwise
        _queryDate = new Date( recurRule1StartDate.getTime() + TestData.const.dayMilliSeconds );
        expect( recurRule1Recurrence2.getTime() ).toEqual( recurRule1.afterInclusive(_queryDate).getTime() );
        // returns null if the given date falls after the recur rule end date
        _queryDate = new Date( recurRule1EndDate.getTime() + TestData.const.dayMilliSeconds );
        expect( recurRule1.afterInclusive(_queryDate) ).toBeNull();
    } );

    it( "returns an iterator of recurrence dates for the recur rule", function () {
        var next, itr = recurRule1.recurrence( recurRule1StartDate, recurRule1EndDate );
        var ary = [];
        while ((next = itr.next()) && !next.done) {
            ary.push(next.value);
        }
        expect( 5 ).toEqual( ary.length  );
        expect( recurRule1StartDate.getTime() ).toEqual( ary[0].getTime() );
        expect( recurRule1Recurrence2.getTime() ).toEqual(  ary[1].getTime() );
        expect( recurRule1Recurrence3.getTime() ).toEqual(  ary[2].getTime() );
        expect( recurRule1Recurrence4.getTime() ).toEqual(  ary[3].getTime() );
        expect( recurRule1Recurrence5.getTime() ).toEqual(  ary[4].getTime() );
    } );

} );