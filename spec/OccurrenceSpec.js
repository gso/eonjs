
describe( 'An Occurence object', function () {

    var recurRule1 = TestData.table[0].recurRule;
    var recurRule1Recurrence2 = TestData.table[0].recurrenceAry[1];  // recurrence after initial start date
    var occurrence = new RecurRuleContainer.Occurrence( recurRule1Recurrence2, recurRule1);

    it( "is created with a validating constructor ", function () {
        expect( occurrence instanceof RecurRuleContainer.Occurrence ).toBe( true ) ;
        expect( function () {
                new RecurRuleContainer.Occurrence( "should be a date", recurRule1 );
            } )
            .toThrowError( TypeError );
        expect( function () {
                new RecurRuleContainer.Occurrence(recurRule1Recurrence2, "should be a recur rule");
            } )
            .toThrowError( TypeError );
        expect( recurRule1 ).toEqual( occurrence.recurRule );
        expect( recurRule1Recurrence2 ).toEqual( occurrence.date );
    } );

});