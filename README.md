

# EonJS (alpha release)

Provides an abstraction layer over existing recur rule libraries (currently [moment-recur](https://github.com/c-trimm/moment-recur) and [rrule.js](https://github.com/jakubroztocil/rrule)) and a recur rule container class as for example might be the basis of a calendar application.
        
### Download

Required:
 
* __[wu.js](https://fitzgen.github.io/wu.js/)__ 
* __[moment-recur](https://github.com/c-trimm/moment-recur)__ recur rule library which is dependant on __[moment](http://momentjs.com/)__ (optional)
* __[rrule](https://github.com/jakubroztocil/rrule)__ recur rule library (optional)

### Support

* Initial class [documentation](https://gso.bitbucket.io/EonJS/index.html)
* See also [example-*.html](https://bitbucket.org/gso/eonjs/src/master/)
* [Issue tracker](https://bitbucket.org/gso/eonjs/issues)

### Development

__package.json__ should install all necessary dev. and run dependencies, install issues with __moment-recur__ can be bypassed by reinstalling with `--force`.

To add support for an external recur rule library subclass [AbstractRecurRule.js](https://bitbucket.org/gso/eonjs/src/master/src/es6/gso/eonjs/AbstractRecurRule.js).

### Licence

GPL-3.0-or-later