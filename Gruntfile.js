
// Builds debug and distribution versions of source code

module.exports = function(grunt) {
    "use strict";

    grunt.loadNpmTasks( 'grunt-contrib-clean' );
    grunt.loadNpmTasks( 'grunt-run' );
    grunt.loadNpmTasks( 'grunt-contrib-copy' );

    grunt.initConfig( {
        clean: {
            doc: [ './doc' ]
        },
        run: {
            build_documentation: {
                cmd: './node_modules/.bin/jsdoc',
                args: [ '--configure', './jsdoc.conf.json' ]
            }
        },
        copy: {
            external_js: {  // place copies of external libs in 'js/' dir
                files: [
                    { expand: true, nonull: true, cwd: 'node_modules/moment/min/',
                        src: 'moment.min.js', dest: 'js/' },
                    { expand: true, nonull: true, cwd: 'node_modules/moment-recur/',
                        src: 'moment-recur.js', dest: 'js/' },
//                    { expand: true, nonull: true, cwd: 'node_modules/rrule/dist/esm/src/',
//                        src: 'rrule.js', dest: 'js/' },
                    { expand: true, nonull: true, cwd: 'node_modules/wu/dist',
                        src: 'wu.js', dest: 'js/' }
                ]
            }
//            dist: {  // node package
//                files: [
//                ]
//            }
        }
    } );

    grunt.registerTask( 'init', [
        'copy:external_js'
    ] );
    grunt.registerTask( 'doc', [
        'run:build_documentation'
    ] );
//    grunt.registerTask( 'build', [
//    ] );
//    grunt.registerTask( 'test', [
//    ] );
    grunt.registerTask( 'clean', [
        'clean:js', 'clean:doc', 'clean:dist'
    ] );
    grunt.registerTask( 'default', [] );
};
